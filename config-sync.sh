#! /bin/bash


repo_directory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
repo_synclist=$repo_directory/synclist
repo_homedir=$repo_directory/homedir
user_homedir=$HOME

function confirm {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        *)
            false
            ;;
    esac
}

function ensure_directory {
    mkdir -p $repo_config
    touch $repo_synclist
}

function pull_config {
    echo "Using config directory $user_homedir"
    echo "Using destination $repo_homedir"

    IFS=$'\n'
    set -f
    for line in $(cat < "$repo_synclist") ; do
        if [ "$(echo $line | grep "^#")" ] ; then
            continue # is a comment line
        elif [ "$(echo $line | grep "^;")" ] ; then
            continue # is a disabled sync target
        else
            echo $line >> .tmpsync
        fi
    done

#    shopt -s dotglob
    rsync -arRvh --dry-run --files-from=./.tmpsync $user_homedir $repo_homedir
    confirm "Complete the sync? [y/N]" && rsync -arR --files-from=./.tmpsync $user_homedir $repo_homedir
    tree -a $repo_homedir
    rm .tmpsync
}

function push_config {
    echo "[Currently not implemented!]"
}

function print_usage {
    echo -e "Usage: config-sync.sh [pull|push]"
    echo -e "\tpull: clones the current user config into the repository"
    echo -e "\tpush: applies the repository config to the current user"
}


if [ $# -ne 1 ]; then
    print_usage
else
    if [ "$1" == "pull" ]; then
        ensure_directory
        pull_config
    elif [ "$1" == "push" ]; then
        ensure_directory
        push_config
    else
        print_usage
    fi
fi


