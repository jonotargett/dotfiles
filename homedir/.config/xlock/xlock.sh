#!/bin/sh

icon=$HOME/.config/xlock/icon.png
name=/tmp/screen
ext=.png
bg=$name$ext
thumb=$name-thumb$ext
mod=$name-mod$ext

transparent=00000000
insidecolor=00000000
ringcolor=2b457dff
ringhlcolor=3862c2ff
ringdlcolor=a82667ff
ringvercolor=00ff00ff
ringwrongcolor=$ringdlcolor
clockcolor=6699d2ff
linecolor=00000000

# optionally, get the colours from Xresources
bgcolor="$(xgetres xrdb.background)"
bgcolor="ffffff"
insidecolor="$(xgetres xrdb.background)c3"
ringcolor="$(xgetres xrdb.color0)ff"
ringhlcolor="$(xgetres xrdb.color6)ff"
ringdlcolor="$(xgetres xrdb.color13)ff"
ringvercolor="$(xgetres xrdb.color2)ff"
ringwrongcolor="$(xgetres xrdb.color5)ff"
clockcolor="$(xgetres xrdb.foreground)ff"

lock=""
unlock=""   #""
text=""
radius=90
ringwidth=40

radius=240
ringwidth=60
clock="-k"
font="Overpass Nerd Font Mono:Thin"
#font="Ubuntu Nerd Font Mono:Light"
font="Lekton Nerd Font Mono"
#font="ArulentSansMono Nerd Font Mono"
dings="UbuntuMono Nerd Font"
clockx=699	#700 for centre
clocky=400

mode="-i $mod"
#mode="-c $bgcolor"

#backlight=$(xbacklight)
#xbacklight -set 1

scrot "$bg" -t 4
#convert "$thumb" -scale 5000% -quality 100 "$mod"
#convert "$thumb" -level 20% -sample 1000% "$mod"
convert "$thumb" -fill black -colorize 35% -sample 2500% "$mod"
#convert "$mod" "$icon" -gravity center -composite -matte "$mod"

i3lock -n $clock $mode --indicator --radius=$radius --ring-width=$ringwidth \
 --ringcolor=$ringcolor --ringvercolor=$ringvercolor --keyhlcolor=$ringhlcolor \
 --ringwrongcolor=$ringwrongcolor --bshlcolor=$ringdlcolor --separatorcolor=$transparent \
 --insidecolor=$insidecolor --insidevercolor=$insidecolor --insidewrongcolor=$insidecolor --linecolor=$transparent \
 --veriftext="$unlock" --wrongtext="$lock" --noinputtext="" --locktext="" --lockfailedtext="" \
 --timecolor=$clockcolor --datecolor=$clockcolor --time-font="$font" --date-font="$font" --timesize=80 --datesize=20 --timepos="$clockx:$clocky" --datestr="%A, %-d %B %Y" \
 --wrong-font="$dings" --wrongsize=200 --wrongcolor=$ringdlcolor --verif-font="$dings" --verifsize=200 --verifcolor=$ringvercolor

#xbacklight -set $backlight

