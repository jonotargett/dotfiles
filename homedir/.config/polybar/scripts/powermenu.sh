#!/bin/bash

## Created By Aditya Shakya

lock=""
logout=""
reboot=""
shutdown=""

location="-location 3 -yoffset 75 -xoffset -15 -width 13"
format="-hide-scrollbar -line-padding 20 -padding 20 -lines 4"
opts="$lock lock|$logout logout|$reboot reboot|$shutdown shutdown"

MENU="$(rofi -config ~/.config/rofi/powermenu -sep "|" -dmenu -i -p 'system' $location $format <<< $opts)"

case "$MENU" in
	*lock) /home/jono/.config/xlock/xlock.sh ;;
        *logout) i3-msg exit ;;
        *reboot) systemctl reboot ;;
        *shutdown) systemctl -i poweroff
esac



#MENU="$(rofi -config ~/.config/rofi/powermenu -sep "|" -dmenu -i -p 'System' -location 3 -yoffset 60 -xoffset -28 -width 15 -hide-scrollbar -line-padding 4 -padding 20 -lines 4 <<< " Lock| Logout| Reboot| Shutdown")"
#            case "$MENU" in
#                *Lock) ~/.xlock/xlock.sh ;;
#                *Logout) openbox --exit;;
#                *Reboot) systemctl reboot ;;
#                *Shutdown) systemctl -i poweroff
#            esac

