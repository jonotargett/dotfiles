#!/usr/bin/env bash

colorindex=12
ucolor=$(xgetres color$colorindex)

play=
pause=
stop=
u="%{-u}"

function player_choice {
    if [ "$1" == "" ]; then
        echo ""
    else
        echo "-p $1"
    fi
}

function player_status {
    continue=1
    while [ $continue -eq 1 ]; do
        sleep 1

        player=$(cat /tmp/playerapp)
        players=$(playerctl -l 2> /dev/null)

        if [ "$players" == "" ]; then
            player=""
            echo "" # spit out a newline so that the polybar gets cleared
        else
            while read -r line; do
                playing=$(playerctl -p $line status)
                #echo "... $line $playing ..."
                if [ "$player" == "" ]; then
                    player=$line
                else
                    if [ "$playing" == "Playing" ]; then
                        player=$line
                    fi
                fi
            done <<< "$players"

            playerchoice=$(player_choice $player)

            underline=""
            icon=""
            status=$(playerctl $playerchoice status 2> /dev/null)
            if [ "$status" == "Playing" ]; then
                underline="%{u$ucolor}"
                icon=$play
            fi
            if [ "$status" == "Paused" ]; then
                underline="%{u$ucolor}"
                icon=$pause
            fi
            if [ "$status" == "Stopped" ]; then
                icon=$stop
            fi

            echo -e "  $underline$icon $player$u"

            check_icon_url &
        fi
        echo $player > /tmp/playerapp
    done
}

function check_icon_url {
    playerchoice=$(player_choice $(cat /tmp/playerapp))

    icon=$(playerctl $playerchoice metadata 2> /dev/null | grep -oE "mpris:artUrl': <'[^,>]+" | cut -d\' -f3)
    if [ "$icon" == "" ]; then
        i="do nothing"
    else
        url=$(curl -s $icon | grep "meta http-equiv=\"refresh\"" | grep -oE "url=[^\"]+" | cut -d= -f2)
        prevurl=$(cat /tmp/playericon.url)

        if [ "$prevurl" == "$url" ]; then
            prevurl=""
        else
            curl -s -o /tmp/playericon.png $url
            echo $url > /tmp/playericon.url
        fi
    fi
}


if [ $# -eq 0 ]; then
    player_status
elif [ $# -eq 1 ]; then
    playerchoice=$(player_choice $(cat /tmp/playerapp))

    if [ "$1" == "play-pause" ]; then
        playerctl $playerchoice play-pause
    fi
    if [ "$1" == "next" ]; then
        playerctl $playerchoice next
    fi
    if [ "$1" == "previous" ]; then
        playerctl $playerchoice previous
    fi
    if [ "$1" == "what" ]; then
        title=$(playerctl $playerchoice metadata title)
        artist=$(playerctl $playerchoice metadata artist)
        album=$(playerctl $playerchoice metadata album)
        notify-send "$title" "$artist - $album" -i /tmp/playericon.png -t 1000
    fi
fi
