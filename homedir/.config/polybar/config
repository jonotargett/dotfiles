;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;==========================================================

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

; Define fallback values used by all module formats
format-foreground = #FF0000
format-background = #00FF00
format-underline =
format-overline =
format-spacing =
format-padding =
format-margin =
format-offset =

[colors]
p0 = ${xrdb:color0:#222}
p1 = ${xrdb:color1:#222}
p2 = ${xrdb:color2:#222}
p3 = ${xrdb:color3:#222}
p4 = ${xrdb:color4:#222}
p5 = ${xrdb:color5:#222}
p6 = ${xrdb:color6:#222}
p7 = ${xrdb:color7:#222}

p8 = ${xrdb:color8:#222}
p9 = ${xrdb:color9:#222}
p10 = ${xrdb:color10:#222}
p11 = ${xrdb:color11:#222}
p12 = ${xrdb:color12:#222}
p13 = ${xrdb:color13:#222}
p14 = ${xrdb:color14:#222}
p15 = ${xrdb:color15:#222}

background = ${xrdb:background:#222}
background-alt = ${xrdb:color0:#444}
foreground = ${xrdb:foreground:#222}
foreground-alt = ${xrdb:color7:#555}

primary = ${xrdb:color8:#000}
secondary = #e60053
alert = #bd2c40

################################################################################
################################################################################
############                        TOP BAR i3                        ##########
################################################################################
################################################################################


[bar/top]
;monitor = ${env:MONITOR:HDMI-1}
monitor-strict = false
override-redirect = false
width = 100%
height = 60
offset-y = 0
radius = 0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 7
line-color = #0f0
border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 0

module-margin-left = 0
module-margin-right = 0

;https://github.com/jaagr/polybar/wiki/Fonts
font-0 = "UbuntuMono Nerd Font:size=22;4"
font-1 = "UbuntuMono Nerd Font:size=46;9"
font-2 = "UbuntuMono Nerd Font:size=38;9"

modules-left = arch i3 pad6 xwindow
modules-center =
modules-right = arrow0 pad0 memory pad0 cpu arrow1 pad1 temperature arrow2 pad2 wlan arrow3 pad3 battery arrow4 pad4 pulseaudio playerctl-status arrow5 pad5 date arrow6 powermenu
separator =

wm-restack = i3
;override-redirect = true
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left =
click-middle =
click-right =
;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev
double-click-left =
double-click-middle =
double-click-right =
cursor-click = pointer
cursor-scroll = ns-resize


tray-position = right
;tray-position = none
tray-detached = true
tray-scale = 1
tray-maxsize = 1000
tray-padding = 0
;tray-background = #0063ff
tray-offset-y = 1740

################################################################################
################################################################################
############                       MODULES ARROWS                     ##########
################################################################################
################################################################################


[module/arrow0]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = ${colors.p0}
content-background = ${colors.background}

[module/arrow1]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = ${colors.p1}
content-background = ${colors.p0}

[module/arrow2]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = ${colors.p2}
content-background = ${colors.p1}

[module/arrow3]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = ${colors.p3}
content-background = ${colors.p2}

[module/arrow4]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = ${colors.p4}
content-background = ${colors.p3}

[module/arrow5]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = ${colors.p5}
content-background = ${colors.p4}

[module/arrow6]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = ${colors.background}
content-background = ${colors.p5}

################################################################################
################################################################################
############                      MODULES PADDING                     ##########
################################################################################
################################################################################

[module/pad0]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-background = ${colors.p0}

[module/pad1]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-background = ${colors.p1}

[module/pad2]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-background = ${colors.p2}

[module/pad3]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-background = ${colors.p3}

[module/pad4]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-background = ${colors.p4}

[module/pad5]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-background = ${colors.p5}

[module/pad6]
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-background = ${colors.background}

################################################################################
################################################################################
############                       MODULES A-Z                      ############
################################################################################
################################################################################


[module/networkspeedup]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
interface = wlp2s0
label-connected = "%upspeed:7%"
format-connected = <label-connected>
format-connected-prefix = " "
format-connected-prefix-foreground =  ${colors.foreground}
format-connected-foreground =  ${colors.foreground}
format-connected-background = ${colors.p2}

[module/networkspeeddown]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
interface = wlp2s0
label-connected = "%downspeed:7%"
format-connected = <label-connected>
format-connected-prefix = "  "
format-connected-prefix-foreground =  ${colors.foreground}
format-connected-foreground =  ${colors.foreground}
format-connected-background = ${colors.p2}

[module/xwindow]
type = internal/xwindow
label = %title:0:60:...%

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted = %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

; Separator in between workspaces
; label-separator = |

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

;label-active = 
;label-occupied = 
;label-urgent = 
;label-empty = 

; focused = Active workspace on focused monitor
;label-focused = %index%
label-focused = 
;alt-icons:       
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
;label-unfocused = %index%
label-unfocused = 
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

; Separator in between workspaces
; label-separator = |


[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #f90000
format-background = ${colors.p0}
label = CPU %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #4bffdc
format-background = ${colors.p0}
label = Memory %percentage_used%%

[module/wlan]
type = internal/network
interface = wlp2s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = ${colors.p10}
format-connected-background = ${colors.p2}
label-connected = %essid%

;format-disconnected =
format-disconnected = <label-disconnected>
format-disconnected-underline = ${colors.p2}
format-disconnected-background = ${colors.p2}
label-disconnected = %ifname% disconnected
label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.foreground-alt}

[module/eth]
type = internal/network
interface = enp3s0
interval = 3.0

format-connected-underline = #55aa55
format-connected-prefix = ""
format-connected-prefix-foreground = ${colors.foreground-alt}
format-connected-background = ${colors.p2}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 5

date =
date-alt = " %Y-%m-%d"

time = "%H:%M"
time-alt = "%H:%M:%S"
format-prefix = 
;format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}
format-background = ${colors.p5}
format-underline = ${colors.p13}

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume>
label-volume =  %percentage%%
label-volume-foreground = ${root.foreground}
label-volume-background = ${colors.p4}
label-volume-underline = ${colors.p12}

label-muted =  muted
label-muted-foreground = #666
label-muted-background = ${colors.p4}
label-muted-underline = ${colors.p4}

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

click-right = /home/jono/.config/polybar/scripts/powermenu.sh

[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = VOL
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = sound muted

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT1
adapter = ADP1
full-at = 94

format-charging = <animation-charging> <label-charging>
format-charging-underline = ${colors.p11}
format-charging-background = ${colors.p3}

#format-discharging = <animation-discharging> <label-discharging>
format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${colors.p3}
format-discharging-background = ${colors.p3}

format-full-prefix = ""
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${self.format-charging-underline}
format-full-background = ${colors.p3}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = ${colors.foreground-alt}
animation-discharging-framerate = 750

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-underline = ${colors.p1}
format-background = ${colors.p1}

format-warn = <ramp> <label-warn>
format-warn-underline = ${colors.p9}
format-warn-background = ${colors.p1}

label = %temperature-c%
label-warn = %temperature-c%
label-foreground = ${colors.foreground}
label-warn-foreground = ${colors.foreground}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-alt}

[module/arch]
type = custom/text
;content = ""
;content = ""
;content = ""
content = "%{T3}  %{T-}"
;alt icons = 
content-padding = 0
content-background = ${colors.background}
content-foreground = ${colors.foreground}
click-left = /home/jono/.config/polybar/scripts/searchmenu.sh

[module/powermenu]
type = custom/text
;content = "襤"
content = " %{T3} 襤 %{T-} "
;content-font = 2
content-padding = 0
content-background = ${colors.background}
content-foreground = ${colors.foreground}
click-left = /home/jono/.config/polybar/scripts/powermenu.sh
;click-right = /home/jono/.config/polybar/scripts/pmenu_1


[module/playerctl-status]
type = custom/script
exec = /home/jono/.config/polybar/scripts/playerctl.sh
format-background = ${colors.p4}
format-foreground = ${colors.foreground}
tail = true

click-left = /home/jono/.config/polybar/scripts/playerctl.sh play-pause
click-right = /home/jono/.config/polybar/scripts/playerctl.sh next
click-middle = /home/jono/.config/polybar/scripts/playerctl.sh what
