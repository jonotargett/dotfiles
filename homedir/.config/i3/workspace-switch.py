#!/usr/bin/python3

import subprocess
import sys
import json
import math
import os
from os.path import expanduser
from tempfile import TemporaryFile
from pprint import pprint
from enum import Enum

def get_workspace():
    handle = subprocess.Popen(["i3-msg","-t","get_workspaces"], stdout=subprocess.PIPE)
    output = handle.communicate()[0]
    data = json.loads(output.decode())
    data = sorted(data, key=lambda k: k['num'])
    for i in data:
        if(i['focused']):
            return (i['num'], i['output'])

def get_workspaces():
    handle = subprocess.Popen(["i3-msg","-t","get_workspaces"], stdout=subprocess.PIPE)
    output = handle.communicate()[0]
    data = json.loads(output.decode())
    data = sorted(data, key=lambda k: k['num'])
    arr = []
    for i in data:
        arr.append(i['num'])
    return arr

def get_tree():
    handle = subprocess.Popen(['i3-msg', '-t', 'get_tree'], stdout=subprocess.PIPE)
    output = handle.communicate()[0]
    data = json.loads(output.decode())
    #pprint(data)

    nn = get_node_names(data)
    #pprint(nn)
    return nn

def get_node_names(element):
    ret = {}
    for node in element.get('nodes'):
        key = node.get('num')
        if key is None:
            key = node.get('name')
        ret[key] = get_node_names(node)

    for node in element.get('floating_nodes'):
        key = node.get('num')
        if key is None:
            key = node.get('name')
        ret[key] = get_node_names(node)

    return ret


class Direction(Enum):
    NONE = 0,
    PREV = -1,
    NEXT = 1

def get_target_workspace(direction):
    current, output  = get_workspace()

    tree = get_tree()
    workspace_size = len(tree[output]['content'][current].keys())

    active_workspaces = get_workspaces()
    current_index = active_workspaces.index(current)
    new_index = current_index
    new_workspace = current
    if direction == Direction.PREV:
        new_index -= 1
        #new_workspace -= 1 ## dont allow creating backwards
    elif direction == Direction.NEXT:
        new_index += 1
        if workspace_size > 0:
            new_workspace += 1      # only allow creating a new workspace if the current one has stuff on it

    # if its possible to jump to an existing workspace, do so (skip empty in-betweens)
    if new_index in range(len(active_workspaces)):
        new_workspace = active_workspaces[new_index]

    return new_workspace


def move(workspace):
    subprocess.Popen(["i3-msg","move container to workspace "+str(workspace)], stdout=subprocess.PIPE)
def jump(workspace):
    subprocess.Popen(["i3-msg","workspace "+str(workspace)], stdout=subprocess.PIPE)

if __name__ == '__main__':
    #jump(Direction.PREV)
    #jump(Direction.NEXT)
    print(sys.argv)

    if len(sys.argv) > 2:
        target = -1

        if sys.argv[2] == 'next':
            target = get_target_workspace(Direction.NEXT)
        if sys.argv[2] == 'prev':
            target = get_target_workspace(Direction.PREV)

        if sys.argv[1] == 'go':
            jump(target)
        if sys.argv[1] == 'move':
            # dont let the last window
            move(target)
            jump(target)

    exit(0)
